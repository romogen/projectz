import csv


class DATA_nn:

    def __init__(self, user_number):
        self.user_number = user_number
        self.raw_data = []
        self.initialize_raw_data()

        self.raw_user_train_data = [self.raw_data[x] for x in range(0, int(0.8 * len(self.raw_data)))]
        self.raw_user_test_data = []
        self.raw_other_users_test_data = []
        self.initialize_test_data_raw()

        self.data_30_min_window_10_slide_train = self.window_data_slide(30, 10, self.raw_user_train_data)
        self.raw_user_test_30_window_10_slide = self.window_data_slide(30, 10, self.raw_user_test_data)
        self.raw_other_users_test_30_window_10_slide = [[x[0], self.window_data_slide(30, 10, x[1])]
                                                        for x in self.raw_other_users_test_data]

    def initialize_raw_data(self):
        with open('../data/dnsSummary_user' + str(self.user_number) + '.pcap.csv', 'r') as f:
            reader = csv.reader(f)
            tmp_raw_data = list(reader)
        for line in tmp_raw_data:
            if line[18] != '1':
                self.raw_data.append(line)


    def initialize_test_data_raw(self):
        all_users = [292, 301, 303, 305, 306, 308, 316, 334, 341, 343, 348, 354, 372, 387, 392]
        for user_num in all_users:
            curr_raw_data = []
            with open('../data/dnsSummary_user' + str(user_num) + '.pcap.csv', 'r') as f:
                reader = csv.reader(f)
                curr_raw_data = list(reader)
            raw_data_with_header = [curr_raw_data[0]]
            for i in range(int(0.8*len(curr_raw_data)), len(curr_raw_data)):
                raw_data_with_header.append(curr_raw_data[i])
            if self.user_number == user_num:
                self.raw_user_test_data = raw_data_with_header
            else:
                self.raw_other_users_test_data.append([user_num, raw_data_with_header])

    @staticmethod
    def window_data_slide(window_size, slide_size, raw_data):
        curr_index = 0
        reserve_index = 1
        ans = []

        while curr_index <= len(raw_data) - window_size:
            curr_index = reserve_index
            curr_index += slide_size
            reserve_index = curr_index
            curr_window = []
            while curr_index < reserve_index + window_size:
                curr_window.append(raw_data[curr_index])
                curr_index += 1
            if len(curr_window) != 0:
                ans.append(curr_window)
        return ans
