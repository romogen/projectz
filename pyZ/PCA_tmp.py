from sklearn.decomposition import PCA
from Data import DATA
import feature_extraction.FeatureHandlerRawData as FHR
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D


def run_PCA_plot_test():
    UDT = DATA(303)
    train, train_tag, test, test_tag = FHR.get_feature_vector_list(UDT, 150)
    pca = PCA(n_components=3)
    pca.fit(train)
    X = pca.transform(train)

    other_test = []
    for i in range(0, len(test_tag)):
        if test_tag[i] == 1:
            other_test.append(test[i])
    T = pca.transform(other_test)

    fig = pyplot.figure()
    ax = fig.add_subplot(111, projection='3d')

    T_seq_x = [t[0] for t in T]
    T_seq_y = [t[1] for t in T]
    T_seq_z = [t[2] for t in T]

    X_seq = [x[0] for x in X]
    Y_seq = [x[1] for x in X]
    Z_seq = [x[2] for x in X]

    ax.scatter(X_seq, Y_seq, Z_seq, c='b')
    ax.scatter(T_seq_x, T_seq_y, T_seq_z, c='r')
    pyplot.show()


def run_PCA_plot_test_2d():
    UDT = DATA(303)
    train, train_tag, test, test_tag = FHR.get_feature_vector_list(UDT, 150)
    pca = PCA(n_components=2)
    pca.fit(train)
    X = pca.transform(train)

    other_test = []
    for i in range(0, len(test_tag)):
        if test_tag[i] == 1:
            other_test.append(test[i])
    T = pca.transform(other_test)

    T_seq_x = [t[0] for t in T]
    T_seq_y = [t[1] for t in T]

    X_seq = [x[0] for x in X]
    Y_seq = [x[1] for x in X]

    pyplot.scatter(T_seq_x, T_seq_y, c='r')
    pyplot.scatter(X_seq, Y_seq, c='b')
    pyplot.show()


# class PCA_C:
#     def __init__(self, n_components=None):
#         self.n_components=n_components
#
#     def transform(self, train, test):
#         myPCA = PCA(n_components=self.n_components)
#         myPCA.fit(train)
#         return myPCA.transform(train), myPCA.transform(test)

run_PCA_plot_test_2d()