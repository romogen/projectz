## Here we want to get a sample contains multiple DNS request in windowed raw data and return a feature value
import numpy
import csv
import time
from pathlib import Path
from sklearn.preprocessing import OneHotEncoder


# number 18
def AAAA_percent(window_data):
    AAAA_counter = 0
    total_req = 0
    for line in window_data:
        if line[18] == '0':
            if line[9] == '28':
                AAAA_counter += 1
            total_req += 1
    return (AAAA_counter / total_req) * 100


def AA_percent(window_data):
    AAAA_counter = 0
    total_req = 0
    for line in window_data:
        if line[18] == '0':
            if line[9] == '1':
                AAAA_counter += 1
            total_req += 1
    return (AAAA_counter / total_req) * 100


# Added
def average_time_response(window_data):
    number_of_added = 0
    counter = 0
    for line in window_data:
        if line[26] != '':
            counter += float(line[26])
            number_of_added += 1
    return counter / number_of_added


# added
# add one hot !
def day_in_week(window_data):
    epoch_time = window_data[0][1]
    time_obj = time.gmtime(float(epoch_time))
    return int(time_obj.tm_wday)


#added
# add one hot ?
def finish_hour_in_minutes(window_data):
    epoch_time = window_data[0][1]
    time_obj = time.gmtime(float(epoch_time))
    return int(time_obj.tm_hour) * 60 + int(time_obj.tm_min)


# added
def duration_in_10_seconds(window_data):
    time_int_seconds = float(window_data[len(window_data) - 1][1]) - float(window_data[0][1])
    return time_int_seconds / 10


# added
def number_of_query_addresses(window_data):
    founded = []
    for line in window_data:
        if line[10] not in founded:
            founded.append(line[10])
    return len(founded)


def query_addresses_vector(raw_data):
    founded = []
    x = 0
    for line in raw_data:
        if x == 0:
            x = 1
            continue
        if line[10] not in founded:
            founded.append(line[10])
    return founded


def tf_query_vector(window_data, query_vector):
    tf_vector = numpy.zeros(len(query_vector))
    for line in window_data:
        if line[10] in query_vector:
            tf_vector[query_vector.index(line[10])] += 1
    return [x / len(window_data) for x in tf_vector]


def get_sorted_tf_vec(user_data_object):
    vec = query_addresses_vector(user_data_object.raw_data)
    tf_vec = tf_query_vector(user_data_object.raw_data, vec)
    return [x for _, x in sorted(zip(tf_vec, vec), reverse=True)]


def get_top_tf_vector_list(UDT, top_num):
    top_200_vec = get_sorted_tf_vec(UDT)
    top_200_vec = top_200_vec[0:min(top_num, len(top_200_vec) - 1)]

    # tf train
    tf_200_vec_list_train = [tf_query_vector(window, top_200_vec)
                             for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    tf_200_vec_list_user_test = [tf_query_vector(window, top_200_vec)
                                 for window in UDT.raw_user_test_30_window_10_slide]
    tf_200_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            tf_200_vec_list_other_test.append(tf_query_vector(window, top_200_vec))

    train_tag = numpy.zeros(len(UDT.data_30_min_window_10_slide_train))
    zeros_tag = numpy.zeros(len(UDT.raw_user_test_30_window_10_slide))
    ones_tag = numpy.ones(len(tf_200_vec_list_other_test))
    test_tag = numpy.concatenate((zeros_tag, ones_tag))

    return numpy.array(tf_200_vec_list_train), train_tag, numpy.array(tf_200_vec_list_user_test+tf_200_vec_list_other_test), test_tag


def get_day_vector_list(UDT):
    # tf train
    day_vec_list_train = [day_in_week(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [day_in_week(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(day_in_week(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_finish_hour_vector_list(UDT):
    # tf train
    day_vec_list_train = [finish_hour_in_minutes(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [finish_hour_in_minutes(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(finish_hour_in_minutes(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_duration_vector_list(UDT):
    # tf train
    day_vec_list_train = [duration_in_10_seconds(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [duration_in_10_seconds(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(duration_in_10_seconds(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_query_numbers_vector_list(UDT):
    # tf train
    day_vec_list_train = [number_of_query_addresses(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [number_of_query_addresses(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(number_of_query_addresses(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_average_time_vector_list(UDT):
    # tf train
    day_vec_list_train = [average_time_response(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [average_time_response(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(average_time_response(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_AAAA_vector_list(UDT):
    # tf train
    day_vec_list_train = [AAAA_percent(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [AAAA_percent(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(AAAA_percent(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_AA_vector_list(UDT):
    # tf train
    day_vec_list_train = [AA_percent(window)
                          for window in UDT.data_30_min_window_10_slide_train]

    # tf test
    day_vec_list_user_test = [AA_percent(window)
                              for window in UDT.raw_user_test_30_window_10_slide]
    day_vec_list_other_test = []
    for user_windows in UDT.raw_other_users_test_30_window_10_slide:
        for window in user_windows[1]:
            day_vec_list_other_test.append(AA_percent(window))

    return numpy.array(day_vec_list_train), numpy.array(day_vec_list_user_test + day_vec_list_other_test)


def get_feature_vector_list(UDT, top_num):
    tf_train, tf_train_tag, tf_test, tf_test_tag = numpy.array(get_top_tf_vector_list(UDT, top_num))
    day_train, day_test = get_day_vector_list(UDT)
    finish_hour_train, finish_hour_test = get_finish_hour_vector_list(UDT)
    duration_train, duration_test = get_day_vector_list(UDT)
    query_number_train, query_number_test = get_query_numbers_vector_list(UDT)
    average_time_train, average_time_test = get_average_time_vector_list(UDT)
    AAAA_train, AAAA_test = get_AAAA_vector_list(UDT)
    AA_train, AA_test = get_AA_vector_list(UDT)

    new_vec_train = []
    new_vec_test = []

    for i in range(0, len(tf_train)):
        feature_row = tf_train[i]
        feature_row = numpy.append(feature_row, [day_train[i], finish_hour_train[i], duration_train[i], query_number_train[i], average_time_train[i], AA_train[i], AAAA_train[i]])
        new_vec_train.append(feature_row)

    for i in range(0, len(tf_test)):
        feature_row = tf_test[i]
        feature_row = numpy.append(feature_row, [day_test[i], finish_hour_test[i], duration_test[i], query_number_test[i], average_time_test[i], AA_test[i], AAAA_test[i]])
        new_vec_test.append(feature_row)

    return new_vec_train, tf_train_tag, new_vec_test, tf_test_tag
