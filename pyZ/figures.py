import csv
from matplotlib import pyplot

your_list = []
with open('../tpf_fpr.csv', 'r') as f:
    reader = csv.reader(f)
    your_list = list(reader)

print(your_list)

x_list = []
y_list = []

for arr in your_list:
    x_list.append(arr[0])
    y_list.append(arr[1])

pyplot.scatter(x_list, y_list)
pyplot.show()
pyplot.ylim([0,1])
pyplot.xlim([0,1])