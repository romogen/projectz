from keras.layers import Input, Dense
from keras.models import Model
import feature_extraction.FeatureHandlerRawData as FHR
import numpy
from Data_nn import DATA_nn


class AA_C:

    def __init__(self, userNum):
        self.UDT = DATA_nn(userNum)
        self.top_tf = FHR.get_sorted_tf_vec(self.UDT)
        self.top_tf = self.top_tf[0:500]
        self.encoding_dim = 32
        self.autoencoder = None
        self.encoder = None
        self.decoder = None

    def get_trained_model(self, train_data, test_data): # , test_data
        # this is the size of our encoded representations
        self.encoding_dim = 32  # 32 floats -> compression of factor 24.5, assuming the input is 784 floats
        td = numpy.vstack(train_data)
        test_d = numpy.vstack(test_data)
        # this is our input placeholder
        input_img = Input(shape=(500,))
        # "encoded" is the encoded representation of the input
        encoded = Dense(self.encoding_dim, activation='linear')(input_img)
        # "decoded" is the lossy reconstruction of the input
        decoded = Dense(500, activation='linear')(encoded)

        # this model maps an input to its reconstruction
        self.autoencoder = Model(input_img, decoded)

        # this model maps an input to its encoded representation
        self.encoder = Model(input_img, encoded)

        # create a placeholder for an encoded (32-dimensional) input
        encoded_input = Input(shape=(self.encoding_dim,))
        # retrieve the last layer of the autoencoder model
        decoder_layer = self.autoencoder.layers[-1]
        # create the decoder model
        self.decoder = Model(encoded_input, decoder_layer(encoded_input))

        self.autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        # print(train_data.shape)
        self.autoencoder.fit(td, td ,
                        epochs=5,
                        batch_size=256,
                        shuffle=True,
                             validation_data=(test_d, test_d))

        return self.autoencoder

    def get_data_transformed(self):
        train = []
        for window in self.UDT.data_30_min_window_10_slide_train:
            train.append(self.window_transform(window))

        test_uesr = []
        for window in self.UDT.raw_user_test_30_window_10_slide:
            test_uesr.append(self.window_transform(window))

        test_other = []
        for usr in self.UDT.raw_other_users_test_30_window_10_slide:
            for window in usr[1]:
                test_other.append(self.window_transform(window))

        test = test_uesr + test_other
        train_tag = numpy.zeros(len(train))
        test_user_tag = numpy.zeros(len(test_uesr))
        test_other_tag = numpy.ones(len(test_other))
        test_tag = numpy.concatenate((test_user_tag, test_other_tag))

        return train, train_tag, test, test_tag

    def window_transform(self, window):
        ans_vec = numpy.zeros(len(self.top_tf))
        for line in window:
            if line[10] in self.top_tf:
                indx = self.top_tf.index(line[10])
                ans_vec[indx] = 1
        return ans_vec

