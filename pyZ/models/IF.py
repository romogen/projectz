# This is the IsolationForest model python file
from sklearn.ensemble import IsolationForest
import feature_extraction.FeatureHandlerRawData as FHR
from Data import DATA
# from feature_selection.PCA import PCA_C


def run_ISOLATION_FOREST(userNum):
    UDT = DATA(userNum)
    # myP = PCA_C(10)
    train, train_tag, test, test_tag = FHR.get_feature_vector_list(UDT, 17)
    # train, test = myP.transform(train, test)
    ISF = IsolationForest(max_samples=500,n_estimators=200, contamination=0.07)
    ISF.fit(train, train_tag)
    test_pred = ISF.predict(test)
    return test_pred, test_tag
