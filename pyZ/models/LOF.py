import feature_extraction.FeatureHandlerRawData as FHR
from Data import DATA
from sklearn.neighbors import LocalOutlierFactor
from feature_selection.PCA import PCA_C

def run_LOF(userNum, nn):
    UDT = DATA(userNum)
    # myP = PCA_C(10)
    train, train_tag, test, test_tag = FHR.get_feature_vector_list(UDT, 17)
    # train, test = myP.transform(train, test)
    ISF = LocalOutlierFactor(n_neighbors=nn)
    ISF.fit(train)
    test_pred = ISF.predict(test)
    return test_pred, test_tag
