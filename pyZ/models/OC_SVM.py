from sklearn.svm import OneClassSVM
import feature_extraction.FeatureHandlerRawData as FHR
from Data import DATA


def run_OneClass_SVM():
    UDT = DATA(292)
    train, train_tag, test, test_tag = FHR.get_feature_vector_list(UDT, 17)
    ISF = OneClassSVM()
    ISF.fit(train, train_tag)
    test_pred = ISF.predict(test)
    return test_pred, test_tag