def acc_by_threshold(pred_raw, th):  # anomaly score = 1 if *not* anomaly and -1 if anomaly
    ans = []
    for pred in pred_raw:
        if pred >= th:
            ans.append(0)
        else:
            ans.append(1)
    return ans


def get_actual_pred(pred_raw):
    return acc_by_threshold(pred_raw, 0)


def estimate_tpr_fpr(pred, tag):
    acc_pos = 0
    acc_neg = 0
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    for i in range(0, len(pred)):
        if pred[i] == tag[i]:
            if pred[i] == 0:  # is the actual user (positive)
                tp += 1
            else:
                tn += 1
        else:
            if pred[i] == 0:  # is the other user (negative)
                fp += 1
            else:
                fn += 1
    tpr = tp / (tp+fn)
    fpr = fp / (tn+fp)
    return tpr, fpr
