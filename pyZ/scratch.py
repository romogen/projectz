import models.IF
import prediction_processing as PP

outputs = []
for userN in [292, 301, 303, 305, 306, 308, 316, 334, 341, 343, 348, 354, 372, 387, 392]:
    # pred, tag = models.LOF.run_LOF(userN, 15)
    # pred, tag = models.OC_SVM.run_OneClass_SVM()
    pred, tag = models.IF.run_ISOLATION_FOREST(userN)
    pred_0_1 = PP.get_actual_pred(pred)
    tpr, fpr = PP.estimate_tpr_fpr(pred_0_1, tag)
    outputs.append([tpr, fpr])
    print("user num = "+ str(userN) +" | tpr = " + str(tpr) + " | fpr = " + str(fpr))
print("uf")


# PCA.run_PCA()